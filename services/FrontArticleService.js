export default {
  async getArticleContents(category, slug) {
    let article = await import(`~/articles/${category}/${slug}.md`)
    return article
  },

  async getAllArticles() {
    const fm = require("front-matter")

    let article_list_json = await import('~/articles/article_list.json'),
        article_list = article_list_json.default

    // TODO: wrap this in a try/catch
    let articles = Object.keys(article_list).map(article_category => {
      // get the array of that category's slugs
      let article_category_list = article_list[article_category]

      // return an array of that category's ArticleService requests
      return article_category_list.map(async article_slug => {

        // get the article based on current article_category and article_slug
        let article_markdown = await this.getArticleContents(article_category, article_slug)
        // return the parsed markdown
        let article = fm(article_markdown.default)
        article.attributes.endpoint = `/courtside/${article_category}/${article_slug}`

        return article
      })
    }).reduce((current_article_category, previous_article_category) => {
      // flatten the multi-dimensional array of articles
      return current_article_category.concat(previous_article_category)
    })

    return await Promise.all(articles)
  },

  async getArticlesByCategory(category) {
    if (typeof category !== 'string') {
      throw new Error('Pass a string for the category argument.')
      return []
    }

    const fm = require("front-matter")

    let article_list_json = await import('~/articles/article_list.json'),
        article_list = article_list_json.default[category]

    if (article_list instanceof Array === false) { return [] }

    try {
      let articles = article_list.map(async article_slug => {
          // get the article based on current article_category and article_slug
          let article_markdown = await this.getArticleContents(category, article_slug)
          // return the parsed markdown
          let article = fm(article_markdown.default)
          article.attributes.endpoint = `/courtside/${category}/${article_slug}`

          return article
      })

      return await Promise.all(articles)
    } catch (error) {
      throw new Error(error)
      return []
    }
  },

  async getArticleCategories() {
    let article_list_json = await import('~/articles/article_list.json'),
        article_list = article_list_json.default

    return Object.keys(article_list)
  }
}
