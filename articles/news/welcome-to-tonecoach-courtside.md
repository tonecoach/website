---
name: welcome-to-tonecoach-courtside
title: Welcome to ToneCoach Courtside
published: December 5, 2019
updated: December 5, 2019
description: Courtside is ToneCoach's brand new, official blog. We look forward to posting about music education, theory, production, and more!
thumbnail: /img/banner.png
---

![ToneCoach logo banner](/img/banner.png)

There's only one feeling better than working on the projects that you love: *completing milestones* on those projects!

We have a lot of dreams at ToneCoach, and we like to dream big. All of our dreams revolve around helping music artists, engineers, and producers become the best they can be at their art. We are working on developing tools for ear training and keeping track of your progress at your craft, connecting users with remote and in-person mentors, personalized courses on every genre, instrument, and music-related topic at every level, and so much more.

More important than our dreams, though, is our connection to the people who use our platform. That's where the Courtside blog comes in!

## Who, or what, is ToneCoach? 🎵
ToneCoach is a brand new project, so you probably haven't heard of us yet. What started off as a simple ear training app quickly grew into a vision of a platform for modern music education. There are many moving parts that work to create a new song and release it to the world to share, and there's a lot of talent, education, practice, and dedication to making each of those moving parts work.

There's the unknown, underground band or artist that is great at song writing but can't mix or master their recordings.

There's the EDM producer that makes great mixes from the content in his or her sample library, but doesn't know how to market their creations.

There's the DJ that wants to learn music production, but doesn't know what kind of equipment is needed to create new originals in the genre that they play live.

There's the mixing or mastering engineer that has tons of useful plugins and analog gear on their chain, but isn't sure how to start their own studio and get clients.

There's the passionate (insert your job title here) who wants to quit their day job and start a record label or management company because that's what their dream has been for years.

There's the young student who has a music theory question on brass, woodwind, or string instruments.

There's the individual that wants to become the best they can be with their musical talent.

We want to help all of these musicians, audio engineers, and music producers fill their knowledge gaps, hone their skills, and connect with their peers to accomplish their music goals - no matter their current skill level. Whatever those goals are, you can achieve them, and we are so excited to be working on a platform that will help launch you forward and give you tools to continue improving.

## So what's "Courtside", then? 🏀
The voice of ToneCoach. We want to use this blog, and eventually a Courtside podcast, to release helpful articles on music education, music trends, advice from experts, plugin/software tips and reviews, industry news, and more. It's the true mouthpiece for everything we aim to do at ToneCoach.

## How can I keep in touch? 🔔
ToneCoach is new, but we **are** on social media! Twitter is where we plan to be most active right now, and you can check our feed out and follow us [@TeamToneCoach](https://twitter.com/TeamToneCoach). Facebook and Instagram may come later, but for now Twitter is the best place to reach us and keep in touch.

You can also sign up for our email newsletter! Visit the [tone.coach home page](https://tone.coach) and you'll find a signup form for the platform updates and early access email list.

## We look forward to helping you solve your music problems and become the best you can be in your music career! 👋
